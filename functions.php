<?php

// Site-specific customizations

// "Permanent" plugins are manually updated

function get_my_permanent_plugins() {
  return [
    'advanced-custom-fields-pro/acf.php',
    'duplicator-pro/duplicator-pro.php',
    'tangible-admin-features/tangible-admin-features.php',

    'tangible-blocks/tangible-blocks.php',
    'tangible-blocks-pro/tangible-blocks-pro.php',

    'wp-migrate-db-pro/wp-migrate-db-pro.php',
    'wp-migrate-db-pro-media-files/wp-migrate-db-pro-media-files.php',
    'wp-migrate-db-pro-theme-plugin-files/wp-migrate-db-pro-theme-plugin-files.php',
  ];
}

// Remove admin bar from frontend

add_action('after_setup_theme', function() {
  show_admin_bar(false);
});

// Clear admin dashboard
add_action('wp_dashboard_setup', function() {

  global $wp_meta_boxes;

  $remove = array(
    'normal' =>
      [
        // 'dashboard_activity',
        'dashboard_incoming_links',
        'dashboard_right_now',
        'dashboard_plugins',
        'dashboard_recent_drafts',
        'dashboard_recent_comments',
        'dashboard_site_health',
        'dashboard_php_nag',

        // The Events Calendar
        'tribe_dashboard_widget',

        'bbp-dashboard-right-now',
        'woocommerce_dashboard_recent_reviews',
        'woocommerce_dashboard_status',

        // Postman SMTP - They're likely to change this
        'example_dashboard_widget'
      ],
    'side' =>
      [
        'dashboard_primary',
        'dashboard_secondary',
        'dashboard_quick_press',
      ],
  );

  foreach ($remove as $area => $widgets) {
    foreach ($widgets as $widget) {
      unset($wp_meta_boxes['dashboard'][$area]['core'][$widget]);
      unset($wp_meta_boxes['dashboard'][$area]['high'][$widget]);
    }
  }

}, 99);

/**
 * Remove plugin deactivate and delete actions from plugins list
 */
function my_prevent_remove_permanent_plugins($actions, $plugin_file, $plugin_data, $context) {

  // $plugin_data = [ 'Name' => '' ]

  $permanent_plugins = get_my_permanent_plugins();

  if (in_array($plugin_file, $permanent_plugins)) {
    unset($actions['deactivate']);
    unset($actions['delete']);
  }

  return $actions;
}

add_filter('plugin_action_links', 'my_prevent_remove_permanent_plugins', 10, 4);
add_filter('network_admin_plugin_action_links', 'my_prevent_remove_permanent_plugins', 10, 4);

/**
 * Remove plugin update check
 */
add_filter('site_transient_update_plugins', function($value) {

  $plugin_entries = get_my_permanent_plugins();

  foreach ($plugin_entries as $plugin_entry) {
    if ( isset( $value->response[ $plugin_entry ] ) ) {
      unset( $value->response[ $plugin_entry ] );
    }
  }
  return $value;
}, 10, 1);
